package com.lolaage.tsr.dals.ds;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;

public class LolaageDataSource extends SimpleDriverDataSource {
	public Logger log = Logger.getLogger(LolaageDataSource.class);

	private String url;
	private String username;
	private String password;
	private Connection conn;
	
	public LolaageDataSource(String url,String username,String password)
	{
		this.url = url;
		this.username = username;
		this.password = password;
	}
	
	@Override
	public Connection getConnection() throws SQLException {
		//TODO:后续需要改造为使用连接池。
		if(conn == null || conn.isClosed())
		{
			if (log.isDebugEnabled())
				log.debug("开启了一个新连接");
			conn = DriverManager.getConnection(url, username, password);
		}
		return conn;
	}
	public Connection getCurrConnection() {
		return conn;
	}

	@Override
	public Connection getConnection(String arg0, String arg1)
			throws SQLException {
		return getConnection();
	}

}
