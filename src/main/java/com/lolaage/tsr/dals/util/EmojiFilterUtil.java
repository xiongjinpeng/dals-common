package com.lolaage.tsr.dals.util;

import org.apache.commons.lang.StringUtils;


/**
 * @ClassName : EmojiFilterUtil.java
 * @Description:
 * @Modification history Nov 15, 2014 11:13:25 AM
 * @Autor : driver.deng driver_deng@163.com
 * @Date : Nov 15, 2014 11:13:25 AM
 * @version :
 * 
 */
public class EmojiFilterUtil {

	/**过滤掉字符串中的emoji 表情符*/
	public static String filterEmoji(String source) {
		if (!isContainEmoji(source)) {
			return source;
		}
		StringBuilder buf = null;
		int len = source.length();
		for (int i = 0; i < len; i++) {
			char codePoint = source.charAt(i);
			if (isNotEmojiCharacter(codePoint)) {
				if (buf == null) {
					buf = new StringBuilder(source.length());
				}
				buf.append(codePoint);
			}
		}
		if (buf == null) {
			return null;
		} else {
			return buf.toString();
		}
	}

	/**判断字符串是否包含emoji表情符*/
	public static boolean isContainEmoji(String source) {
		if (StringUtils.isBlank(source)) {
			return false;
		}
		int len = source.length();
		for (int i = 0; i < len; i++) {
			char codePoint = source.charAt(i);
			if (!isNotEmojiCharacter(codePoint)) {
				return true;
			}
		}
		return false;
	}

	/**判断是否是emoji 表情符*/
	private static boolean isNotEmojiCharacter(char codePoint) {
		return (codePoint == 0x0) || (codePoint == 0x9) || (codePoint == 0xA)
				|| (codePoint == 0xD)
				|| ((codePoint >= 0x20) && (codePoint <= 0xD7FF))
				|| ((codePoint >= 0xE000) && (codePoint <= 0xFFFD))
				|| ((codePoint >= 0x10000) && (codePoint <= 0x10FFFF));
	}
	
	public static void main(String[] args) {
		
	/*	StringBuffer buf=new StringBuffer(10);
		buf.append("abc");
		System.out.println(buf.length());*/
		
		 String s = "<body>😄213这是一个有各种内容的消息,  Hia Hia Hia !!!! xxxx@@@...*)!" +
	                "(@*$&@(&#!)@*)!&$!)@^%@(!&#. 😄👩👨], ";
	        String c = filterEmoji(s);
	        System.out.println(s.equals(c));
	       // String expected = "<body>213这是一个有各种内容的消息,  Hia Hia Hia !!!! xxxx@@@...*)" +
	        //        "!(@*$&@(&#!)@*)!&$!)@^%@(!&#. ], ";
	      //  assertEquals(expected, c);
//	        assertSame(c, expected);
	       // assertSame(expected, "<body>213这是一个有各种内容的消息,  Hia Hia Hia !!!! xxxx@@@...*)" +
	        //        "!(@*$&@(&#!)@*)!&$!)@^%@(!&#. ], ");
	       // assertSame(c, Utils.filterEmoji(c));
	}
}
