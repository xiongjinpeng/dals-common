package com.lolaage.tsr.dals.common;

import java.util.Map;

/**
 * @ClassName : EntityResult.java
 * @Description: 实体解析结果
 * @Modification history Nov 5, 2014 3:26:23 PM
 * @Autor : driver.deng  driver_deng@163.com
 * @Date  : Nov 5, 2014 3:26:23 PM
 * @version : 
 *
 */
public class EntityResult {
	/**对像解析结果*/
	private Map<String, Object> entityMap=null;
	/**默认使用自定义主键，兼容2bulu 系统*/
	private GeneratorType generatorType=GeneratorType.ASSIGNED;
	
	public Map<String, Object> getEntityMap() {
		return entityMap;
	}
	public void setEntityMap(Map<String, Object> entityMap) {
		this.entityMap = entityMap;
	}
	public GeneratorType getGeneratorType() {
		return generatorType;
	}
	public void setGeneratorType(GeneratorType generatorType) {
		this.generatorType = generatorType;
	}
	
	
}
