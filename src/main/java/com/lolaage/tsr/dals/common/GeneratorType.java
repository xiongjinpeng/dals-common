package com.lolaage.tsr.dals.common;


/**
 * @ClassName : TeneratorType.java
 * @Description: 主键类型
 * @Modification history Nov 5, 2014 3:11:02 PM
 * @Autor : driver.deng  driver_deng@163.com
 * @Date  : Nov 5, 2014 3:11:02 PM
 * @version : 
 *
 */
public enum GeneratorType {
	IDENTITY{/**mysql生成器生成，由auto_increment 确发*/
		public byte getValue() {
			return 0;
		}
	},
	ASSIGNED{/**用户自定义*/
		public byte getValue() {
			return 1;
		}
	};
	public abstract byte getValue();

	public static GeneratorType getTeneratorType(byte value) {
		GeneratorType rslt = null;
		switch (value) {
		case 0:
			rslt = IDENTITY;
			break;
		case 1:
			rslt = ASSIGNED;
			break;
		}
		return rslt;
	}
}
