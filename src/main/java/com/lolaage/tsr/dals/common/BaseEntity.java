package com.lolaage.tsr.dals.common;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * 
 * @author brain 
 */
public abstract class BaseEntity {
	public String getIdColumnName() {
		return "id";
	}
	
	public String toString() {
		StringBuffer str = new StringBuffer(this.getClass().getSimpleName() + ":[");
		Field[] fields = this.getClass().getDeclaredFields();
		for (Field f : fields) {
			String fieldName = f.getName();
			String getterName = "get" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
			try {
				Method getter = this.getClass().getDeclaredMethod(getterName);
				Object value = getter.invoke(this);
				str.append(fieldName).append("=").append(value).append(", ");
			} catch (Exception e) {
				continue;
			}
		}
		if (str.length() > 1) {
			str.delete(str.length() - 2, str.length());
			str.append("]");
		}
		return str.toString();
	}
	
	
	/**
	 * 获取排序字段的方法。
	 * 例如要按number字段排序，则返回" number "
	 * 如果要按number,sequeue排序，则返回" number,sequeue "
	 * 子类重新实现。
	 * @return
	 */
	public String getSqlOrderBy()
	{
		return "";
	}
}


