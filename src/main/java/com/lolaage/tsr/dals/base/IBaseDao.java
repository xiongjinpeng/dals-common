package com.lolaage.tsr.dals.base;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.CallableStatementCallback;
import org.springframework.jdbc.core.CallableStatementCreator;

import com.lolaage.tsr.dals.common.PageBean;



/**
 * @author 邓鹏
 * @Project Farseer-cc_console
 * @Date 2012-12-22
 * @ReMarks 
 */
public interface IBaseDao {
	/**
	 * 插入数据
	 * @param entity
	 * @throws DaoException
	 */
	public Long save(Object entity);
	/**
	 * 修改数据
	 * @param entity
	 * @throws DaoException
	 */
	public void update(Object entity);
	
	/**
	 * 物理删除
	 * @param entity
	 * @throws DaoException
	 */
	public void delete(Object entity);
	
	
	/**
	 * 根据IDS批量删除数据。
	 * IDS是以英文逗号分隔的ID字符串。
	 * @param clazz
	 * @param ids
	 */
	public void deleteByIds(Class clazz,String ids);
	
	/**
	 * 根据主键查询
	 * @param identity
	 * @return
	 */
	public Object queryByIdentity(Class entityClass,Long identity);
	
	/**
	 * 执行SQL语句
	 * @param sql
	 * @param param
	 * @return
	 */
	public Object executeSql(String sql,Object[] param);
	
	/**
	 * 传入对应参数进行查询
	 * @param model
	 * @return
	 */
	public List query(Object model);
	
	/**
	 * 传入对应参数进行分页查询
	 * @param model
	 * @param pageBean
	 * @return
	 */
	public List query(Object model, PageBean pageBean);
	/**
	 * 传入对应参数进行分页查询
	 * @param model
	 * @param pageBean
	 * @param queryByLikeFields 传入的字段用like方式查询
	 * @return
	 */
	public List query(Object model, PageBean pageBean, String... queryByLikeFields);
	
	/**
	 * @see {@link #queryBySql(String, Object[])}
	 * 通过sql进行查询，返回的必定是传入的对象类型
	 * @param entityClass
	 * @param sql
	 * @param args
	 * @return
	 */
	public List queryBySql(Class entityClass, String sql, Object[] args);
	/**
	 * 获取单个对象
	 * @param entityClass
	 * @param sql
	 * @param args
	 * @return
	 */
	public <T> T queryObjectBySql(Class<T> entityClass, String sql, Object[] args);
	/**
	 * @see {@link #queryBySql(String, Object[])}
	 * 通过sql进行分页查询，返回的必定是传入的对象类型
	 * 不建议用,容易出错
	 * @param entityClass
	 * @param sql
	 * @param args
	 * @param pageBean
	 * @return
	 */
	@Deprecated
	public List queryBySql(Class entityClass, String sql, Object[] args, PageBean pageBean);
	
	/**
	 * 
	 * 
	 * @param entityClass 必传参数
	 * @param sql  原始参数
	 * @param countSql 统计sql
	 * @param args 查询参数
	 * @param pageBean 分页
	 * @return
	 */
	public List queryBySql(Class entityClass, String sql, String countSql,Object[] args, PageBean pageBean);
	
	/**
	 * 根据单个属性查询集合。
	 * @param entityClass
	 * @param propName
	 * @param propValue
	 * @param isLike  是否采用like方式查询，如果用 = 号，则传入false.
	 * @return
	 */
	public List queryByOneProperty(Class entityClass,String propName,Object propValue,boolean isLike);
	
	
	/**
	 * 查询总记录数
	 * @param sql
	 * @param array
	 * @return
	 */
	public long queryCount(String sql, Object[] array);
	
	/**
	 * 调用存储过程
	 * @param <T>
	 * @param csc
	 * @param action
	 * @return
	 */
	public <T> T callable(CallableStatementCreator csc, CallableStatementCallback<T> action);
	
	public DataSource getDatasource();
}
